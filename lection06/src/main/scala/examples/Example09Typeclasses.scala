package examples

object Example09Typeclasses1 {

  trait Order[A] {
    def lt(a: A, b: A): Boolean
  }

  object OrderInstances {
    implicit val intOrder: Order[Int] = (a, b) => a.compareTo(b) < 0

    implicit val stringOrder: Order[String] = (a, b) => a.compareTo(b) < 0
  }
}

object Order {
  import Example09Typeclasses1.Order

  def insertionSort[A](xs: List[A])(implicit ord: Order[A]): List[A] = {
    def insert(y: A, ys: List[A]): List[A] =
      ys match {
        case List() => y :: Nil
        case z :: zs =>
          if (ord.lt(y, z)) y :: z :: zs
          else z :: insert(y, zs)
      }

    xs match {
      case List()  => Nil
      case y :: ys => insert(y, insertionSort(ys)(ord))
    }
  }

}

object Example09Typeclasses2 {

  import Example09Typeclasses1.OrderInstances._

  Order.insertionSort(List(1, 2, 3))
  Order.insertionSort(List("a", "b", "c"))
}

object Example09Typeclasses3 {
  import Example09Typeclasses1._

  implicit class OrderOps[A](private val xs: List[A]) extends AnyVal {
    def insertionSort(implicit o: Order[A]): List[A] =
      Order.insertionSort(xs)
  }
}

object OrderSyntaxExample {
  import Example09Typeclasses1.OrderInstances._
  import Example09Typeclasses3._

  List(1, 2, 3).insertionSort
  List("a", "b", "c").insertionSort
}

package typeclasses

object Example01Monoids extends App  {
  trait Semigroup[A] {
    def combine(x: A, y: A): A
  }

  object Semigroup {
    @inline
    def apply[A](implicit inst: Semigroup[A]): Semigroup[A] =
      inst
  }

  implicit class SemigroupOps[A](private val a: A) extends AnyVal {
    def |+|(b: A)(implicit semigroup: Semigroup[A]): A =
      semigroup.combine(a, b)
  }

  trait Monoid[A] extends Semigroup[A] {
    def empty: A
  }

  type HttpStatusRange = Int => Boolean
  object HttpStatusRange {
    val `1xx`: HttpStatusRange = status => status < 200
    val `2xx`: HttpStatusRange = status => status < 300 && status >= 200
    val `3xx`: HttpStatusRange = status => status < 400 && status >= 300
    val `4xx`: HttpStatusRange = status => status < 500 && status >= 400
    val `5xx`: HttpStatusRange = status => status >= 400

    def single(status: Int): HttpStatusRange = _ == status
  }

  implicit val httpStatusRangeMonoid: Monoid[HttpStatusRange] = new Monoid[HttpStatusRange] {
    def empty: HttpStatusRange = _ => false
    def combine(x: HttpStatusRange, y: HttpStatusRange): HttpStatusRange = s => x(s) || y(s)
  }

  case class HttpResponse(status: Int, body: String = "", headers: Map[String, String] = Map.empty)

  def validateResponse(res: HttpResponse)(validRange: HttpStatusRange): Either[String, HttpResponse] =
    if (validRange(res.status))
      Right(res)
    else Left(s"Invalid status code ${res.status}")

  println(validateResponse(HttpResponse(200))(HttpStatusRange.`1xx` |+| HttpStatusRange.`2xx`))
  println(validateResponse(HttpResponse(500))(HttpStatusRange.`1xx` |+| HttpStatusRange.`2xx`))
  println(
    validateResponse(HttpResponse(300))(
      HttpStatusRange.`2xx` |+|
      HttpStatusRange.`4xx` |+|
      HttpStatusRange.single(500)
    )
  )
}
